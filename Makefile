# This is the name that our final kernel executable will have.
# Change as needed.
KERNEL := kernel

# It is highly recommended to use a custom built cross toolchain to build a kernel.
# We are only using "cc" as a placeholder here. It may work by using
# the host system's toolchain, but this is not guaranteed.
CC = clang

# User controllable CFLAGS.
CFLAGS = -Wall -Wextra -pipe -O0 -Wno-unused-parameter -Wno-interrupt-service-routine #-O2 -pipe

# Internal link flags that should not be changed by the user.
INTERNALLDFLAGS :=     \
	-nostdlib\
	-static\
	-z max_page_size=0x1000\
	-Tlink.ld

# Internal C flags that should not be changed by the user.
INTERNALCFLAGS  :=           \
	-Isrc                  \
	-std=gnu11           \
	-fno-stack-protector \
	-fno-stack-check \
	-fno-lto \
	-fno-pic \
	-fno-pie \
	-m64 \
	-march=x86-64 \
	-mabi=sysv \
	-mno-80387 \
	-mno-mmx \
	-mno-sse \
	-mno-sse2 \
	-mno-red-zone \
	-mcmodel=kernel \
	-DAMD64 \
	-ggdb
	# -MMD

# Use find to glob all *.c files in the directory and extract the object names.
CFILES := src/main.c
OBJ    := src/main.o

# Targets that do not actually build a file of the same name.
.PHONY: all clean

# Default target.
all: $(KERNEL)

# Link rules for the final kernel executable.
$(KERNEL): $(OBJ)
	$(CC) $(INTERNALLDFLAGS) $(OBJ) -o $@

# Compilation rules for *.c files.
%.o: %.c
	$(CC) $(CFLAGS) $(INTERNALCFLAGS) -c $< -o $@

# Remove object files and the final executable.
clean:
	rm -rf $(OBJ)

image:
	mv kernel fs
	xorriso -as mkisofs -b limine-cd.bin \
      -no-emul-boot -boot-load-size 4 -boot-info-table \
      --efi-boot limine-eltorito-efi.bin \
      -efi-boot-part --efi-boot-image --protective-msdos-label \
      fs -o test.iso

#image:
	#mv kernel fs
	#fatstuff/target/release/fatstuff
#
run: all image clean
	qemu-system-x86_64 -net none -cdrom test.iso -M q35 -accel kvm -s

install: all image clean
	cp fs/kernel /efi
