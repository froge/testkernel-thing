use std::fs::ReadDir;
use std::io::Write;
use std::io::Read;

fn main() {
    let file = std::fs::OpenOptions::new()
        .read(true)
        .write(true)
        .open("test.img")
        .unwrap();
    file
        .set_len(8000000)
        .expect("you gotta be kiddin' me");
    fatfs::format_volume(&file, fatfs::FormatVolumeOptions::new())
        .unwrap();
    let fs = fatfs::FileSystem::new(&file, fatfs::FsOptions::new())
        .unwrap();
    copy_dir("fs", &fs.root_dir());
}

fn copy_dir<'a, T: fatfs::ReadWriteSeek + 'a>(path: &str, fs: &fatfs::Dir<'a, T>) {
    for i in std::fs::read_dir(path).unwrap() {
        let y = i.unwrap();
        if y.metadata().unwrap().is_dir() {
            let mut new_dir = fs.create_dir(&y.file_name().into_string().unwrap()).unwrap();
            copy_dir(y.path().to_str().unwrap(), &new_dir);
        } else {
            let mut new_file = fs.create_file(&y.file_name().into_string().unwrap()).unwrap();
            let mut old_file = std::fs::OpenOptions::new()
                .read(true)
                .open(y.path()).unwrap();
            let mut old_file_content = Vec::new();
            old_file.read_to_end(&mut old_file_content).unwrap();
            new_file.write_all(&old_file_content);
        }
    }
}
