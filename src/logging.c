void log_error(char *string) {
    printf("[");
    term_fg = 0x00F00000;
    printf("error");
    term_fg = TERM_FG;
    printf("] ");
    println(string);
}

void log_ok(char *string) {
    printf("[ ");
    term_fg = 0x0050a040;
    printf("OK");
    term_fg = TERM_FG;
    printf(" ] ");
    println(string);
}

void log_fatal(char *string) {
    term_bg = 0x00F00000;
    println("FATAL: %s", string);
    term_bg = TERM_BG;
}
