static volatile struct limine_hhdm_request hhdm_request = {
  .id = LIMINE_HHDM_REQUEST,
  .revision = 0,
};

static volatile struct limine_framebuffer_request fb_request = {
  .id = LIMINE_FRAMEBUFFER_REQUEST,
  .revision = 0,
};

static volatile struct limine_memmap_request memmap_request = {
  .id = LIMINE_MEMMAP_REQUEST,
  .revision = 0,
};

static volatile struct limine_boot_time_request boot_time_request = {
  .id = LIMINE_BOOT_TIME_REQUEST,
  .revision = 0,
};

static volatile struct limine_module_request module_request = {
  .id = LIMINE_MODULE_REQUEST,
  .revision = 0,
};

static volatile struct limine_kernel_address_request kernel_address_request = {
  .id = LIMINE_KERNEL_ADDRESS_REQUEST,
  .revision = 0,
};

static volatile struct limine_rsdp_request rsdp_request = {
  .id = LIMINE_RSDP_REQUEST,
  .revision = 0,
};

struct State {
  u64 hhdm_offset;
  struct limine_framebuffer *fb;
  u64 memmap_count;
  struct limine_memmap_entry **memmap_entries;
  u64 boot_time;
  u64 module_count;
  struct limine_file **modules;
  struct RSDPDescriptor2 *rsdp;
  u64 physical_kbase;
  u64 virtual_kbase;
};

static struct State state;

void init_state() {
  state.hhdm_offset = hhdm_request.response->offset;
  state.fb = fb_request.response->framebuffers[0];
  state.memmap_count = memmap_request.response->entry_count;
  state.memmap_entries = memmap_request.response->entries;
  state.boot_time = boot_time_request.response->boot_time;
  state.module_count = module_request.response->module_count;
  state.modules = module_request.response->modules;
  state.physical_kbase = kernel_address_request.response->physical_base;
  state.virtual_kbase = kernel_address_request.response->virtual_base;
  state.rsdp = rsdp_request.response->address;
}