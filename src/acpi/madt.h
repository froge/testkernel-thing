struct MADT {
  struct ACPISDTHeader h;
  u32 lapic_addr;
  u32 flags;
};

struct LAPIC {
  u8 acpi_puid;
  u8 apic_id;
  u32 flags;
};

struct IOAPIC {
  u8 id;
  u8 reserved;
  u32 addr;
  u32 gsibase;
};

struct InterruptSourceOverride {
  u8 source;
  u32 gsi;
  u16 flags;
};

struct IOSAPIC {
  u8 id;
  u8 reserved;
  u32 gsibase;
  u64 addr;
};

void parse_madt(struct MADT *madt) {
  acpi_validate_checksum(madt, madt->h.len);
  u8 *ptr = (u8*)madt + 44;
  u32 offs = 0;
  while (offs < madt->h.len - 44) {
    u8 type = ptr[offs];
    println("%d", type);
    u8 len = ptr[offs + 1];
    println("%d len", len);
    switch (type) {
      case 0:
        println("local APIC found");
        struct LAPIC *lapic = (void*)(ptr + offs + 2);
        break;
      case 2:
        println("ISO found");
        struct InterruptSourceOverride *iso = (void*)(ptr + offs + 2);
        break;
      case 1:
        println("IOAPIC found");
        struct IOAPIC *ioapic = (void*)(ptr + offs + 2);
        break;
      case 6:
        println("IOSAPIC found");
        struct IOSAPIC *iosapic = (void*)(ptr + offs + 2);
        break;
    }
    offs += len;
  }
}
