#include "acpi.h"

int acpi_validate_checksum(void *thing, int len) {
  u8 sum = 0;
  for (int i = 0; i < len; i++) {
    sum += ((u8*)thing)[i];
  }
  if (sum != 0) {
    log_error("ACPI TABLE INVALID");
  }
}

void acpi_init(struct RSDT *rsdt) {
  acpi_validate_checksum(&rsdt->h, rsdt->h.len);
  int entries = (rsdt->h.len - sizeof(rsdt->h)) / 4;
  println("%d", entries);
  u32 sdtpointer = &rsdt->sdtpointer;
  struct ACPITables tables;
  for (int i = 0; i < entries; i++) {
    struct ACPISDTHeader *h = (struct ACPISDTHeader*)(*(u32*)(sdtpointer + 4 * i));
    if (!memcmp(h->signature, "APIC", 4)) {
      tables.madt = h;
    } else if (!memcmp(h->signature, "FACP", 4)) {
      tables.fadt = h;
    } else if (!memcmp(h->signature, "MCFG", 4)) {
      tables.mcfg = h;
    } else if (!memcmp(h->signature, "HPET", 4)) {
      tables.hpet = h;
    }
    print_write_len(h->signature, 4);
    printf("\n");
  }
  parse_madt(tables.madt);
}
