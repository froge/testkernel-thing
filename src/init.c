#ifdef AMD64
#include "arch/amd64/idt.c"
#include "arch/amd64/interrupts.c"

void kernel_preinit_amd64() {
    load_idt();
    if (!quiet) {
        log_ok("idt loaded");
    }
    idt_set_handler(0, divide_by_zero_exception);
    idt_set_handler(1, debug_exception);
    idt_set_handler(2, nmi_exception);
    idt_set_handler(3, breakpoint_exception);
    idt_set_handler(4, overflow_exception);
    idt_set_handler(5, bound_range_exceeded_exception);
    idt_set_handler(6, invalid_opcode_exception);
    idt_set_handler(7, device_not_available_exception);
    idt_set_handler(8, double_fault_exception);
    idt_set_handler(10, invalid_tss_exception);
    idt_set_handler(11, segment_not_present_exception);
    idt_set_handler(12, stack_segment_fault_exception);
    idt_set_handler(13, general_protection_fault_exception);
    idt_set_handler(14, page_fault_exception);
    if (!quiet) {
        log_ok("interrupt handlers set");
    }
    asm("sti");
    if (!quiet) {
        log_ok("interrupts enabled");
    }
}


void kernel_preinit() {
    kernel_preinit_amd64();
}
#endif
