typedef u64 pt_entry;

struct Pagemap {
    pt_entry *pml4t;
};

#define PAGE_PRESENT ((u64)1 << 0)
#define PAGE_WRITABLE ((u64)1 << 1)
#define PAGE_USER_ACCESS ((u64)1 << 2)
#define PAGE_WRITE_THROUGH ((u64)1 << 3)
#define PAGE_NOEXEC ((u64)1 << 63)

struct Pagemap new_pagemap() {
    struct Pagemap pagemap;
    pagemap.pml4t = calloc(512 * sizeof(pt_entry));
    return pagemap;
}

void map_page(struct Pagemap pagemap, u64 phys_addr, u64 virt_addr, u64 flags) {
    // u16 l4_index = (virt_addr >> 21 >> 9 >> 9 >> 9) & 511; //0o777;
    u16 l4_index = (virt_addr >> 21 >> 9 >> 9) & 511; //0o777;
    u16 l3_index = (virt_addr >> 21 >> 9) & 511; //0o777;
    u16 l2_index = (virt_addr >> 21) & 511; //0o777;
    u16 l1_index = (virt_addr << (64 - 21) >> (64 - 21)) / 0x1000; //0o777;
    pt_entry *pml4, *pml3, *pml2, *pml1;
    pml4 = pagemap.pml4t;
    if (!((u64)pml4[l4_index] & PAGE_PRESENT)) {
        pml3 = calloc(0x1000);
        pml4[l4_index] = PAGE_WRITABLE | PAGE_USER_ACCESS | PAGE_PRESENT | (u64)pml3;
    } else {
        pml3 = pml4[l4_index] >> 12 << 12;
    }
    if (!((u64)pml3[l3_index] & PAGE_PRESENT)) {
        pml2 = calloc(0x1000);
        pml3[l3_index] = PAGE_WRITABLE | PAGE_USER_ACCESS | PAGE_PRESENT | (u64)pml2;
    } else {
        pml2 = pml3[l3_index] >> 12 << 12;
    }
    if (!((u64)pml2[l2_index] & PAGE_PRESENT)) {
        pml1 = calloc(0x1000);
        pml2[l2_index] = PAGE_WRITABLE | PAGE_USER_ACCESS | PAGE_PRESENT | (u64)pml1;
    } else {
        pml1 = pml2[l2_index] >> 12 << 12;
    }
    // pml1 = (pt_entry*)pml2[l2_index];
    // if (!((u64)pml1[l1_index] & PAGE_PRESENT)) {
        //pml1[l1_index] = PAGE_PRESENT | (u64)calloc(0x1000);
    //}
    pml1[l1_index] = flags | phys_addr;
}

void load_pagemap(struct Pagemap pagemap) {
    u64 poop = (u64)pagemap.pml4t;
    asm("movq %0, %%cr3;"
            :
            : "r" (poop) : );
}
