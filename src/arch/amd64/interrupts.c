void disable_interrupts() {
    asm("cli");
}

void enable_interrupts() {
    asm("sti");
}


__attribute__((interrupt))
void divide_by_zero_exception(void *test) {
    log_error("exception: #DE");
}

__attribute__((interrupt))
void debug_exception(void *test) {
    log_error("exception: #DB");
}

__attribute__((interrupt))
void nmi_exception(void *test) {
    log_error("exception: nmi");
}

__attribute__((interrupt))
void breakpoint_exception(void *test) {
    log_error("exception: #BP");
}

__attribute__((interrupt))
void overflow_exception(void *test) {
    log_error("exception: #OF");
}

__attribute__((interrupt))
void bound_range_exceeded_exception(void *test) {
    log_error("exception: #BR");
}

__attribute__((interrupt))
void invalid_opcode_exception(void *test) {
    log_error("exception: #UD");
}

__attribute__((interrupt))
void device_not_available_exception(void *test) {
    log_error("exception: #NM");
}

__attribute__((interrupt))
void invalid_tss_exception(void *test, u64 err) {
    log_error("exception: #TS");
}

__attribute__((interrupt))
void segment_not_present_exception(void *test, u64 err) {
    log_error("exception: #NP");
}

__attribute__((interrupt))
void stack_segment_fault_exception(void *test, u64 err) {
    log_error("exception: #SS");
}

__attribute__((interrupt))
void general_protection_fault_exception(void *test, u64 err) {
    log_error("exception: #GP");
}

__attribute__((interrupt))
void page_fault_exception(void *test, u64 err) {
    log_error("exception: #PF");
    println("error code %d", err);
}
__attribute__((interrupt))
void double_fault_exception(void *test, u64 err_code) {
    log_fatal("DOUBLE FAULT");
    for (;;) {
        asm("hlt");
    }
}

__attribute__((interrupt)) void keyboard_interrupt(void *test) {
    println("keyboard int");
}
