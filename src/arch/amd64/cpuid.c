void cpu_vendor_string(char *buf) {
    u64 one, two, three;
    asm volatile("movl $0, %%eax;"
        "cpuid;"
            : "=b" (one), "=c" (two), "=d" (three)
            :
            : "%eax");
    for (int i = 0; i < 4; i++) {
        buf[i] = one >> i * 8;
    }
    for (int i = 0; i < 4; i++) {
        buf[i + 4] = three >> i * 8;
    }
    for (int i = 0; i < 4; i++) {
        buf[i + 8] = two >> i * 8;
    }
}
