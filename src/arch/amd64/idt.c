struct idtr {
    u16 limit;
    u64 base;
}__attribute__((packed));

struct IDTDescr {
    u16 offset_1;
    u16 selector;
    u8 ist;
    u8 tags_attr;
    u16 offset_2;
    u32 offset_3;
    u32 zero;
};

struct idtr idtr = {0};
struct IDTDescr idt[256] = {0};

u16 get_cs() {
    u16 cs;
    asm volatile("movw %%cs, %0;"
        : "=r" (cs)
        :);
    return cs;
}

void idt_set_handler(u16 index, void *handler) {
    u16 cs = get_cs();
    u64 addr = (u64)handler;
    idt[index] = (struct IDTDescr) {
        .offset_1 = (u16)addr,
        .selector = cs,
        .ist = 0,
        .tags_attr = 0x8e,
        .offset_2 = (u16)(addr >> 16),
        .offset_3 = (u32)(addr >> 32),
        .zero = 0
    };
}

void load_idt() {
    for (u16 i = 0; i < 256; i++) {
        idt[i] = (struct IDTDescr) {
            .offset_1 = 0,
            .selector = 0,
            .ist = 0,
            .tags_attr = 0,
            .offset_2 = 0,
            .offset_3 = 0,
            .zero = 0
        };
    }
    idtr.limit = sizeof(idt) - 1;
    idtr.base = (u64)&idt;
    asm volatile ("lidt %0;"
            :
            : "m" (idtr)
            );
}
