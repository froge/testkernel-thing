struct gdtr {
    u16 limit;
    u64 base;
} __attribute__((packed));

struct gdt_desc {
    u16 limit;
    u16 base_low;
    u8 base_mid;
    u8 access;
    u8 granularity;
    u8 base_hi;
} __attribute__((packed));

struct gdtr gdtr;

void load_gdt() {
    struct gdt_desc gdt[] = {
        {0},

    };
}
