u64 term_x = 0;
u64 term_y = 0;
//u32 term_fg = 0xFfFfFfFf;
u32 term_fg = TERM_FG;
u32 term_bg = TERM_BG;
//u32 term_bg = 0x00FF2b41;

void printchar(char ch) {
    if (ch == '\n') {
        term_y++;
        if (term_y * 16 > state.fb->height) {
            term_y = 0;
        }
        term_x = 0;
        return;
    }
    putchar(ch, term_x, term_y, term_fg, term_bg);
    term_x++;
}

void print_write(const char *string) {
    int i = 0;
    for (;;) {
        if (string[i] == '\0')
            break;
        printchar(string[i]);
        i++;
    }
}

void print_write_len(const char *string, int len) {
    int i = 0;
    for (;;) {
        if (i >= len)
            break;
        if (string[i] == 0) {
            i++;
            continue;
        }
        printchar(string[i]);
        i++;
    }
}

static const char conv_tbl[] = "0123456789abcdef";
void printnum(u64 num) {
    char buf[64] = { 0 };
    int i = 63;
    if (num == 0) {
        printchar('0');
        return;
    }
    while (num) {
        buf[i] = '0' + num % 10;
        num /= 10;
        i--;
    }
    buf[i] = '\0';
    print_write_len(buf, 64);
}

void printhex(u64 num) {
    char buf[64] = { 0 };
    int i = 63;
    while (num) {
        buf[i] = conv_tbl[num % 16];
        num /= 16;
        i--;
    }
    print_write_len(buf, 64);
}

void printf(const char *string, ...) {
    va_list ap;
    va_start(ap, string);
    char cur_char;
    for (int i = 0;;i++) {
        cur_char = string[i];
        if (cur_char == '\0') {
            break;
        }
        if (cur_char == '%') {
            i++;
            cur_char = string[i];
            switch (cur_char) {
                case 's':
                    {
                        char *tempstr = va_arg(ap, char*);
                        print_write(tempstr);
                        break;
                    }
                case 'd':
                    printnum(va_arg(ap, u64));
                    break;
                case 'x':
                    printhex(va_arg(ap, u64));
                    break;
            }
        } else {
            printchar(cur_char);
        }
    }
}


void println(const char *string, ...) {
    va_list ap;
    va_start(ap, string);
    char cur_char;
    for (int i = 0;;i++) {
        cur_char = string[i];
        if (cur_char == '\0') {
            break;
        }
        if (cur_char == '%') {
            i++;
            cur_char = string[i];
            switch (cur_char) {
                case 's':
                    {
                        char *tempstr = va_arg(ap, char*);
                        print_write(tempstr);
                        break;
                    }
                case 'd':
                    printnum(va_arg(ap, u64));
                    break;
                case 'x':
                    printhex(va_arg(ap, u64));
                    break;
            }
        } else {
            printchar(cur_char);
        }
    }
    printchar('\n');
}

