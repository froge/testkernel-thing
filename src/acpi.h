struct RSDPDescriptor1 {
  char signature[8];
  u8 checksum;
  char oem_id[6];
  u8 rev;
  u32 rsdt_addr;
} __attribute__ ((packed));

struct RSDPDescriptor2 {
  struct RSDPDescriptor1 rsdp_10;
  
  u32 len;
  u64 xsdt_addr;
  u8 xchecksum;
  u8 reserved[3]
} __attribute__ ((packed));

struct ACPISDTHeader {
  char signature[4];
  u32 len;
  u8 rev;
  u8 checksum;
  char oem_id[6];
  char oem_table_id[8];
  u32 oem_rev;
  u32 creator_id;
  u32 creator_rev;
};

struct XSDT {
  struct ACPISDTHeader h;
  u64 sdtpointer;
};

struct RSDT {
  struct ACPISDTHeader h;
  u32 sdtpointer;
};

struct ACPITables {
  struct MADT *madt;
  struct FADT *fadt;
  struct MCFG *mcfg;
  struct HPET *hpet;
};

#include "acpi/madt.h"
