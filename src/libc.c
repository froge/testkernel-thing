void *memset(void *str, int c, size_t n) {
  for (size_t i = 0; i < n; i++) {
    ((u8*)str)[i] = (u8)c;
  }
  return str;
}

int memcmp(void *s1, void *s2, size_t n) { // TODO this is not libc behavior, stop it, get some help
  for (size_t i = 0; i < n; i++) {
    if (((u8*)s1)[i] != ((u8*)s2)[i]) {
      return 1;
    }
  }
  return 0;
}
