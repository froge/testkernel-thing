#include <stddef.h>

struct Bitmap {
    u64 size;
    u8 *map;
};

struct LinkedList {
    struct LinkedList *last;
    struct LinkedList *next;
};

u8 set_bit(u8 byte, u8 bit) {
    return (byte | ( 1 << bit ));
}
u8 clear_bit(u8 byte, u8 bit) {
    return (byte & ~( 1 << bit ));
}
u8 get_bit(u8 byte, int bit) {
    return (byte & (u8)( 1 << bit )) >> bit;
}

u64 memory_size = 0;

void pmm_init() {
    volatile struct limine_memmap_entry *cur_entry = 0;
    //volatile struct LinkedList *last = 0;
    //volatile struct LinkedList *cur = 0;
    for (u64 i = 0; i<state.memmap_count; i++) {
        cur_entry = state.memmap_entries[i];
        switch (cur_entry->type) {
            case LIMINE_MEMMAP_USABLE: // usable
                /*
                for (u64 addr = cur_entry->base; addr<cur_entry->length + cur_entry->base; addr += 0x1000) {
                    println("%d, %d", addr, cur_entry->length + cur_entry->base);
                    cur = (volatile struct LinkedList*)addr;
                    cur->next = 0;
                    last->next = cur;
                    cur->last = last;
                    last = cur;
                }
                */
                memory_size += cur_entry->length;
                // if (cur_entry->base % 0x1000 != 0) {
                //     println("peepee");
                //     cur_entry->base /= 0x1000;
                //     cur_entry->base += 1;
                //     cur_entry->base *= 0x1000;
                // }
                volatile struct Bitmap *bitmap = (void*)cur_entry->base;
                bitmap->size = (cur_entry->length - sizeof(struct Bitmap)) / 0x1000 / 8;
                bitmap->map = (void*)cur_entry->base + sizeof(struct Bitmap);
                for (u64 u = 0; u < bitmap->size; u++) { // TODO use memset
                    bitmap->map[u] = 0;
                }
                bitmap->map[0] = set_bit(0, 0);

            //case 2: // reserved
            //case 3: // acpi_reclaimable
            //case 4: // acpi_nvs
            //case 5: // bad_memory
            //case 0x1000: // bootloader_reclaimable
            //case 0x1001: // kernel_and_module
            //case 0x1002: // framebuffer
        }
    }
    if (!quiet)
        log_ok("pmm initialized!");
    if (!quiet) {
        println("memory size: %dMiB", memory_size / 1048576);
        println("memory size: %dMB", memory_size / 1000000);
        println("memory size: %d", memory_size);
    }
}

void *pmm_alloc(u64 size_not) {
    u64 size = size_not;
    struct limine_memmap_entry *cur_entry = 0;
    u64 offset = 0;
    u64 pages = 0;
    void *ptr = 0;
    for (u64 i = 0; i<state.memmap_count; i++) {
        cur_entry = state.memmap_entries[i];
        switch (cur_entry->type) {
            case LIMINE_MEMMAP_USABLE: // usable
                {
                    struct Bitmap *bitmap = (void*)cur_entry->base;
                    pages = 0;
                    offset = 1;
                    //println("%d", i);
                    if (size > bitmap->size * 8 * 4096) {
                        continue;
                    }
                    //println("%d", i);
                    for (u64 a = 0; a < bitmap->size; a++) {
                        for (int b = 0; b < 8; b++) {
                            if (get_bit(bitmap->map[a], b)) {
                                offset = a * 8 + (b + 1); // b + 1 so that its at the offset for the next iteration of the loop
                                pages = 1;
                                continue;
                            }
                            if (pages * 4096 >= size) {
                                ptr = offset * 4096 + (void*)cur_entry->base;
                                for (u64 set = 0; set < pages; set++) {
                                    bitmap->map[(offset + set) / 8] = set_bit(bitmap->map[offset / 8], (offset + set) % 8);
                                }
                                //println("pos: %d", i);
                                //println("ptr: %d", offset);
                                //println("pages: %d", pages);
                                //for (int p = 0; p < bitmap->size; p++) {
                                    //if (bitmap->map[p] == 0) {
                                        //break;
                                    //}
                                    ////print_bits(bitmap->map[p]);
                                //}
                                //println("");
                                //println("pos: %d", i);
                                if (!ptr) println("nullptr");
                                return ptr;
                            }
                            pages++;
                        }
                    }
                }
            //case 2: // reserved
            //case 3: // acpi_reclaimable
            //case 4: // acpi_nvs
            //case 5: // bad_memory
            //case 0x1000: // bootloader_reclaimable
            //case 0x1001: // kernel_and_module
            //case 0x1002: // framebuffer
        }
    }
    return 0;
}

void *calloc(u64 size) {
    void *ret = pmm_alloc(size);
    if (!ret) {
        printf("nullptr");
        void *ret = pmm_alloc(size);
        if (!ret) printf("nullptr");
    }
    for (u64 i = (u64)ret; i < (u64)ret + size; i += 8) {
        *(volatile u64*)i = 0;
    }
    return ret;
}

/*
void pmm_free(void *ptr, size_t size) {
    struct stivale2_mmap_entry *cur_entry = 0;
    for (int i = 0; i<tags.memmap->entries; i++) {
        //println("%d", i);
        cur_entry = &tags.memmap->memmap[i];
        switch (cur_entry->type) {
            case 1: // usable
                struct Bitmap *bitmap = cur_entry->base;
                if (cur_entry->base + cur_entry->length >= ptr) {
                    u64 offset = ((u8*)ptr - bitmap->map - bitmap->size) / 4096;
                    //println("freeoffset: %d", offset);
                    for (int set = 0; set < size / 4096; set++) {
                        bitmap->map[(offset + set) / 8] = clear_bit(bitmap->map[offset / 8], (offset + set) % 8);
                    }
                    return;
                }
            //case 2: // reserved
            //case 3: // acpi_reclaimable
            //case 4: // acpi_nvs
            //case 5: // bad_memory
            //case 0x1000: // bootloader_reclaimable
            //case 0x1001: // kernel_and_module
            //case 0x1002: // framebuffer
        }
    }
}
*/
 void *pmm_realloc(void *ptr, u64 cur_size, u64 new_size) {
 }

void print_bits(u8 byte) {
    for (int b = 0; b < 8; b++) {
        printnum((int)get_bit(byte, b));
    }
}
