#ifdef AMD64
#include "../arch/amd64/paging.c"
#endif

void identity_map(struct Pagemap pagemap, u64 start, u64 end, u64 flags) {
    for (u64 i = 0; i < end; i += 0x1000) {
        map_page(pagemap, start + i, start + i, flags);
    }
}

void high_identity_map(struct Pagemap pagemap, u64 start, u64 end, u64 flags) {
    for (u64 i = start; i < end; i += 0x1000) {
        map_page(pagemap, i + state.hhdm_offset, i, flags);
    }
}

void offset_map(struct Pagemap pagemap, u64 start, u64 end, u64 offset, u64 flags) {
    for (u64 i = 0; i < end; i += 0x1000) {
        map_page(pagemap, i + start, i + offset, flags);
    }
}
