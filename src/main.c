#include <stddef.h>
#include <limine.h>
#include <stdarg.h>

typedef uint64_t u64;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t  u8;
typedef int64_t  i64;
typedef int32_t  i32;
typedef int16_t  i16;
typedef int8_t   i8;

u8 quiet = 0;

const u32 TERM_FG = 0x00F0F0F0;
const u32 TERM_BG = 0x00111111;

#include "entry.c"
#include "graphics.c"
#include "psf.c"
#include "term.c"
#include "logging.c"
#include "arch/amd64/cpuid.c"
#include "memory/phys.c"
#include "memory/virt.c"
#include "init.c"
#include "libc.c"
#include "acpi.c"

void kmain() {
    init_state();
    psf_init((struct PSFHeader*)state.modules[1]->address);
    clear_screen(state.fb, TERM_BG);
    println("hello kernel world!");
    kernel_preinit();
    char buf[13] = {0};
    cpu_vendor_string((char*)&buf);
    println("cpu vendor: %s", &buf);
    println("%d", get_cs());
    println("                               something funny");
    pmm_init();
    println("%dx%d",state.fb->width, state.fb->height);

    struct Pagemap cur_pagemap = new_pagemap();
    struct limine_memmap_entry *cur_entry = 0;
    // map_page(cur_pagemap, 0, 0, PAGE_PRESENT | PAGE_WRITABLE);
    // map_page(cur_pagemap, HIGHER_HALF, 0, PAGE_PRESENT | PAGE_WRITABLE);
    // map_page(cur_pagemap, 4294967296 - 0x1000, 4294967296 - 0x1000, PAGE_PRESENT | PAGE_WRITABLE);
    identity_map(cur_pagemap, 0x1000, 4294967296, PAGE_PRESENT | PAGE_WRITABLE);
    offset_map(cur_pagemap, 0, 4294967296, state.hhdm_offset, PAGE_PRESENT | PAGE_WRITABLE);

    // load_pagemap(cur_pagemap);
     for (int i = 0; i<state.memmap_count; i++) {
         cur_entry = state.memmap_entries[i];
         identity_map(cur_pagemap, cur_entry->base/* / 0x1000 * 0x1000*/, cur_entry->length, PAGE_PRESENT | PAGE_WRITABLE);
         offset_map(cur_pagemap, cur_entry->base /*/ 0x1000 * 0x1000*/, cur_entry->length, state.hhdm_offset + cur_entry->base, PAGE_PRESENT | PAGE_WRITABLE);
     }
    offset_map(cur_pagemap, state.physical_kbase, 0x80000000, state.virtual_kbase, PAGE_PRESENT | PAGE_WRITABLE);
    load_pagemap(cur_pagemap);
    println("pages mapped");

    println("%x", cur_pagemap);
    println("amogus we survivered");
    println("%d", state.boot_time);
    acpi_init(state.rsdp->rsdp_10.rsdt_addr);
    for (;;) {
        asm("hlt");
    }
}
