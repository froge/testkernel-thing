void set_pixel(struct limine_framebuffer *fb, u64 x, u64 y, u32 color) {
    if (x <= fb->width && y <= fb->height) {
        *(u32*)(fb->address + x * 4 + y * fb->pitch) = color;
    }
}

void clear_screen(struct limine_framebuffer *fb, u32 color) {
    for (u64 y = 0; y < fb->height; y++) {
        for (u64 x = 0; x < fb->width; x++) {
            set_pixel(fb, x, y, color);
        }
    }
}
