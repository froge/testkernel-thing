struct PSFHeader {
    u8 magic[2];
    u8 mode;
    u8 charsize;
};

struct PSFHeader *font;

void psf_init(struct PSFHeader *font_) {
    font = font_;
}

void putchar(char c, int x, int y, u32 fg, u32 bg) {
    u8 *glyph = (void*)(font + sizeof(struct PSFHeader) + (c-1) * font->charsize / 4 + 1);
    struct limine_framebuffer *fb = state.fb;
    x *= 9;
    y *= 16;
    for (int cy = 0; cy < 16; cy++) {
        for (int cx = 0; cx < 8; cx++) {
            if (glyph[cy] & (0b10000000 >> cx)) {
                set_pixel(fb, cx + x, cy + y, fg);
            } else {
                set_pixel(fb, cx + x, cy + y, bg);
                //set_pixel(cx + x, cy + y, (glyph[cy] & (0b10000000 >> cx)) * bg);
            }
        }
        set_pixel(fb, x + 8, cy + y, bg);
    }
}
